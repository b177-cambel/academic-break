
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response)=>response.json())
.then((json)=> console.log(`Title: ${json.title}
Body: ${json.body}`) )

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method:'PATCH',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Change title',
		body: 'Change Body'
	})
})
.then((response)=> response.json())
.then((json)=> console.log(json))

const express = require("express");
const app = express();
const port = 4001;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

let users = []

app.post("/sign-up", (req, res) => {
	if(req.body.firstname !== '' && req.body.lastname !== '' && req.body.username !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(`Hi, ${req.body.firstname} ${req.body.lastname}! You're successfully registered! Thank you.`);
	}
	else{
		res.send("Please input all the required data.");
	}
});

app.post("/login", (req, res) => {
	
	let message;
	for(let i = 0; i < users.length;i++){
		if((req.body.username == users[i].username) && (req.body.password == users[i].password)){
			message = `User ${req.body.username} successfully login!`
			break;
		}
		else{
			message = "Invalid username and/or password.";
		}
	}
	res.send(message);
});

app.listen(port, () => console.log(`Server running at port ${port}`));
